---
layout: markdown_page
title: Get started with GitLab
description: Find resources to help you get started with GitLab.
---
{:.no_toc}

- TOC
{:toc}

---

## Executives

Enable your organization to build great software faster, with built-in security and executive-level visibility of the entire software development process.

- [Read how Axway realized a 26x faster DevOps cycle with GitLab  ](https://about.gitlab.com/customers/axway/)
- [See how Paessler AG increased releases by 4x and improved QA speed by 120x ](https://about.gitlab.com/customers/paessler/)
- [Learn how Ticketmaster improved crash-free rates and increased releases by 4x ](https://about.gitlab.com/2017/06/07/continous-integration-ticketmaster/)
- [Schedule a call with our Sales team now ](https://about.gitlab.com/sales/)


## Enterprise Architects

Accelerate and scale software delivery across your organization, while ensuring all projects comply with audit requirements.

- [See how GitLab compares to other tools  ](https://about.gitlab.com/devops-tools/)
- [Learn more about concurrent DevOps ](https://about.gitlab.com/images/press/gitlab-data-sheet.pdf)
- [Read how the Cloud Native Computing Foundation improved efficiency with GitLab  ](https://about.gitlab.com/customers/cncf/)
- [Start your 14-day trial of GitLab Ultimate today  ](https://about.gitlab.com/free-trial/)


## Users

Discover a faster, more efficient process for managing software development.

- [Check our our blog for updates, including what’s new in our latest version ](https://about.gitlab.com/blog/)
- [Find answers in our Documentation  ](https://docs.gitlab.com)
- [Browse our Resources to educate yourself  ](https://about.gitlab.com/resources/)
- [Install Gitlab in 2 minutes  ](https://about.gitlab.com/installation/)


## Contributors

Propose, contribute to, and discuss GitLab features.

- [Explore open-source projects and share your own ](https://gitlab.com/explore)
- [Learn how to contribute to GitLab  ](https://about.gitlab.com/contributing/)


## Educators

Prepare your students for the workforce by introducing them to the latest open-source technology. GitLab’s top-tier SaaS and self-hosted offerings are free to educational institutions and open source projects.

- [Find out more about GitLab for Education  ](https://about.gitlab.com/education/)


## Resellers

Get the information and support you need to succeed in your market as a GitLab reseller.

- [Learn how to become an authorized GitLab reseller ](https://about.gitlab.com/resellers/program/)


## Customers

Reduce time to market and make the most of your GitLab deployment with services such as specialized training, data remediation, implementation services, and more.

- [Discover GitLab’s professional services offerings  ](https://about.gitlab.com/services/)