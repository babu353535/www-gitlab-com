---
layout: markdown_page
title: "Product Vision - Plan"
---

- TOC
{:toc}

The product vision of the Plan stage of the [DevOps lifecycle](/direction/#devops-stages)
in GitLab is to enable all people in an organization to innovate ideas, turn
those ideas into transparent plans, track their execution, adjust the plans
along the way as needed, and evaluate the efficacy of the entire process,
providing actionable insight for continuous improvement. This is enabled through
team members, internal and external stakeholders, and executive sponsors all
collaborating on the same single-source-of-truth artifacts throughout the
application.

- See a high-level
  [timeline-based roadmap view of planned upcoming improvements](https://gitlab.com/groups/gitlab-org/-/roadmap?label_name%5B%5D=Plan&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS).
- See work planned for [the next few milestones (monthly iterations) in detail](https://gitlab.com/groups/gitlab-org/-/boards/706864).

## Representative flows

The Plan stage vision is characterized by a few representative flows.

### Engineers and Designers

A small team of three Engineers, one Designer, and one Engineering Manager works
to [create](/direction/create) a new innovative feature in an established
product, currently planned for four iterations, each being two weeks. They are
halfway through the second iteration, which is captured and tracked in a
[milestone](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=✓&state=opened&label_name[]=milestones)
and in multiple
[issue boards](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=✓&state=opened&label_name[]=boards).
Using a
[burndown chart](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=✓&state=opened&label_name[]=burndown%2Fup%20charts),
they quickly see that that they are at risk of missing the last two of the
twelve issues scoped in the current iteration. The feature has a strict
deadline, since it will be part of a high-profile marketing launch, so they are
able to borrow folks from a different team to complete the two issues on time.

At the end of the iteration, the team demos the feature to the Product Manager
and other stakeholders who are impacted. Together they log additional ideas and
potential improvements in the
[issue tracker](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=✓&state=opened&label_name[]=issues)
for future improvement. They confirm plans for the third and last iteration of
the new feature, and start discussing plans for three new initiatives after
that, using issue boards and
[roadmaps](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=✓&state=opened&label_name[]=roadmaps),
accounting for capacity constraints and other team dynamics.

### Product Managers 

A Product Manager (PM) closely collaborates with the team mentioned above, and
in fact, is part of the team itself. The PM monitors progress in
[issue boards](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=✓&state=opened&label_name[]=boards)
and a
[burndown chart](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=✓&state=opened&label_name[]=burndown%2Fup%20charts),
but is less focused on the actual delivery, which instead is the primary
responsibility of the engineering team. The PM stays in close communication with
internal stakeholders within the company, coordinating the planned demo at the
end of the iteration.

The PM also maintains a long-term
[roadmap](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=✓&state=opened&label_name[]=roadmaps)
for their own responsbile product. In addition to internal stakeholders, the PM
frequently communicates with external and customers, sharing the roadmap, and
adjusting it to fit market conditions and other strategic opportunities, as well
as being aligned with the executive-level product strategy mentioned below.

### Directors

An Engineering Director reporting to the VP of Engineering manages five
engineering managers, who themselves in turn manage individual development
teams. The Director is a new hire, filling out a new position tasked to further
develop the people (engineering managers) and processes of this fast-growing
company. The director uses
[analytics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=✓&state=opened&label_name[]=analytics)
in the form of five customized dashboards based on OKRs (objectives and key
results) that were developed after an initial observation period and signed off
by the VP of Engineering. The analytics confirm the problems re-iterated by team
members in meetings. Testing is a bottleneck for teams. Since there is an OKR to
reduce the defect rate, the Director increases the hiring headcount for test
engineers and works with the team to start an initiative into investigating
possible technology solutions for integrated and automated testing.

The Director's responsibilities also include working with managers to track
status of strategic intiatives in
[boards](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=✓&state=opened&label_name[]=epics)
and
[roadmaps](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=✓&state=opened&label_name[]=roadmaps),
at the GitLab group level, where the scoping is across multiple teams' work.

### Executives

The CEO works with the VPs of Product and Engineering to develop the high-level
product strategy for the next 8 calendar quarters. They create
[epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=✓&state=opened&label_name[]=epics)
and use appropriate labels to indicate that they represent strategic high-level
initiatives. They put start and end dates on the epics, representing their first
pass approximate timelines and order of delivery. The epics are shared across
the company in the
[roadmap](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=✓&state=opened&label_name[]=roadmaps)
view.

The Product and Engineering teams subsequently innovate on those initiatives,
scoping out features and effort estimates which in turn adjust and further
refine the epics themselves.

At the same time, the roadmap of epics are shared with the VPs of Sales,
Marketing, and Operations, and their respective teams. These teams create, track
and socialize their own initiatives using epics as well, that appear in the same
roadmap.

Later on in the year, a strategic acquistion opportunity presents itself to the
executive team and the board of directors. At the board meeting, they review the
roadmap and decide the acquisition fits into the overall company vision and
medium term strategy. They proceed to buy the company. With the acquisition, a
key product feature originally planned is no longer required. Instead, the
Engineering team is now responsible for integrating the technology from the
acquired company. The VPs of Product and Engineering make adjustments to the
roadmap accordingly. At that same time, the VP of Marketing inserts new epics
into the roadmap to produce content and campaigns to explain the impact to
customers (including customers of the acquired company). The VPs of Sales and
Operations inserts epics to train and integrate the new staff from the acquired
company. The entire roadmap is radically changed due to the acquisition, but the
process is controlled and carefully managed with executive oversight and
collaboration.

## Detailed Plan product vision

Each area of the Plan product vision is shown below. It is ever-changing as we
incorporate more ideas from users, customers, and community members.

- See a high-level
  [timeline-based roadmap view of planned upcoming improvements](https://gitlab.com/groups/gitlab-org/-/roadmap?label_name%5B%5D=Plan&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS).
- See work planned for [the next few milestones (monthly iterations) in detail](https://gitlab.com/groups/gitlab-org/-/boards/706864).

### Portfolio management with epics and roadmaps

GitLab portfolio management (together with project management below) allows
different team members and stakeholders throughout your organization to innovate
and collaborate on high-level ideas; turn those into strategic initiatives with
executive sponsorship; scope and estimate work effort required; plan that work
across quarters and years; and execute it in weeks and months.

See [epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=✓&state=opened&label_name[]=portfolio%20management) of planned upcoming improvements.

### Project management with issues and boards

See [epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=✓&state=opened&label_name[]=project%20management) of planned upcoming improvements.

### Agile portfolio and project management

GitLab supports
[enterprise Agile portfolio and project management frameworks, including Scaled Agile Framework (SAFe)](https://about.gitlab.com/solutions/agile-delivery/).

See
[epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=✓&state=opened&label_name[]=Agile)
of planned upcoming improvements.

### Personal workflow management with notifications and todos

GitLab helps you get your work done, personalized to your own specific
workflows, using notifications and todos.

See [epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=✓&state=opened&label_name[]=todos) of planned upcoming improvements.

### Governance with enforced workflows and custom fields

Larger enterprises require enhanced governance in their business management and
software delivery, which can be addressed with enforced workflows and custom
fields.

See [epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=✓&state=opened&label_name[]=governance) of planned upcoming improvements.

### Analytics

GitLab has in-product analytics, helping teams track performance over time,
highligting major problems, and providing the actionable insight to solve them.

See [epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=✓&state=opened&label_name[]=analytics) of planned upcoming improvements.

See Mark Pundsack talk about [value stream management](https://gitlab.com/groups/gitlab-org/-/epics/215).

<figure class="video_container">
<iframe src="https://www.youtube.com/embed/ZgFqyXCsqPY?start=4002" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

### Search

Teams leverage GitLab search to quickly search for relevant content, enabling
stronger intra-team and cross-team collaboration through discovery of all GitLab
data.

See [epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=✓&state=opened&label_name[]=search) of planned upcoming improvements.

### Jira integration

GitLab supports deep Jira integration, allowing teams to use Jira for issue
mangement, but still leveraging the benefits of GitLab source control and other
native GitLab features.

See [epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=✓&state=opened&label_name[]=Jira) of planned upcoming improvements.

## Other areas of interest

- [Quality management](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=✓&state=opened&label_name[]=quality%20management)
- [Requirements management](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=✓&state=opened&label_name[]=requirements%20management)

## How we prioritize

We follow the [general prioritization process](/handbook/product/#prioritization)
of the Product Team. In particular, we consider reach, impact, confidence, and
effort to identify and plan changes in upcoming milestones (monthly iterations).

- See a high-level
  [timeline-based roadmap view of planned upcoming improvements](https://gitlab.com/groups/gitlab-org/-/roadmap?label_name%5B%5D=Plan&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS).
- See work planned for [the next few milestones (monthly iterations) in detail](https://gitlab.com/groups/gitlab-org/-/boards/706864).

## Contributions and feedback

We love community code contributions to GitLab. Read
[this guide](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/development/contributing/index.md)
to get started.

Please also participate in our [issue tracker](https://gitlab.com/gitlab-org/gitlab-ce).
You can file bugs, propose new features, suggest design improvements, or
continue a conversation on any one of these. Simply [open a new issue](https://gitlab.com/gitlab-org/gitlab-ce/issues/new)
or comment on [an existing one](https://gitlab.com/gitlab-org/gitlab-ce/issues).

You can also contact me (Victor) via the channels on [my profile page](https://gitlab.com/victorwu).

## Direction

<%= direction %>

<%= partial("direction/other", :locals => { :stage => "plan" }) %>
