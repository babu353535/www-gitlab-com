---
layout: job_page
title: "IT Operations System Administrator"
---

## Responsibilities

* Work with Security, PeopleOps and Business Operations to develop automated onboarding and offboarding processes
* Develop secure integrations between Enterprise Business Systems and with our Data Lake
* Develop tooling and process to facilitate end-user asset management, provisioning and tracking
* Build API Integrations from the HRIS to third party systems and GitLab.com 
* Triage all IT related questions as they arise 




## Requirements

* Experience administering GSuite
* Automation & Scripting experience deploying software to laptops and other devices - we are a Mac and Linux environment
* Hands-on experience supporting Linux
* Hands on experience working in GCP environment
* Experience working in a cloud native environment
* Design and implement any auditing workflows that are necessary for access groups
* Developing automation workflows and integrations for repetitive or manual tasks
* Very comfortable with SQL and Python
* Configure, build, test, and deploy multiple HR systems integration solutions
* Experience working with Git



## Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/team).

* Next, candidates will be invited to schedule a [screening call](/handbook/hiring/#screening-call) with our Global Recruiters
* Next, candidates will be invited to schedule a first interview with our Director of Business Operations
* Next, candidates will be invited to schedule a second interview with our Senior People Operations Analyst
* Candidates will then be invited to schedule a third interview with one of our Security team members
* Candidates will be then be invited to schedule a call with our Director of Security
* Finally, candidates may be asked to interview with our CEO

Additional details about our process can be found on our [hiring page](/handbook/hiring).
