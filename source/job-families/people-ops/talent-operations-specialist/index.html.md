---
layout: job_page
title: "Talent Operations Specialist"
---

## Responsibilities

* Manage usage and account for ATS (Greenhouse), as well as other partner accounts, such as LinkedIn, PowerToFly, StackOverflow, and other potential partners
* Work with partners for recruitment and branding
* Design and monitor key metrics to evaluate the effectiveness of GitLab's employment practices
* Pull recruiting metrics reports on a weekly, monthly, and quarterly basis
* Pull any ad-hoc reports, some examples: stats for a specific role, source data, salary data, archive reasons, referral data
* Develop recommendations by leveraging data from our ATS
* Review and act on interview feedback surveys
* Review and act on new hire onboarding surveys
* Develop and deliver a disruptive, thoughtful, and engaging employer brand strategy
* Collaborate with managers on vacancies and establish effective recruiting strategies
* Create better internal brand awareness on our People Ops and Recruiting tools
* Lead demos for potential new services, as well as the research and implementation of new services
* Continually elevate our brand by identifying industry best practices, evaluating competitors, and nurturing networks and partnerships
* Create and stage offer letters and contracts for signature with quick turnaround
* Manage high volume hiring pipeline
* Screen, interview, and evaluate candidates
* Assess candidate interest, skills, values, and ability to thrive in an open source culture
* Onboard new hires
* Promote our culture and foster lasting relationships with candidates

## Requirements

* Minimum six months experience providing administrative support for a Human Resources or People Operations team
* Experience in a startup a plus
* Experience working remotely is a plus
* Proven ability to multitask and prioritize workload
* Excellent communication and interpersonal skills
* Desire to learn about Talent Acquisition and Human Resources
* Demonstrated ability to work in a team environment and work collaboratively across the organization
* Proficient in Google Suite
* Willingness to learn and use software tools including Git and GitLab, prior experience with GitLab is a plus.
* Organized, efficient, and proactive with a keen sense of urgency
* Ability to recognize and appropriately handle highly sensitive and confidential information
* Prior experience using an applicant tracking system (ATS), such as Greenhouse, is a plus
* Prior experience using a human resources information system (HRIS), such as BambooHR, is a plus
* Ability to recognize and appropriately handle highly sensitive and confidential information
* You share our values, and work in accordance with those values
* Successful completion of a background check
