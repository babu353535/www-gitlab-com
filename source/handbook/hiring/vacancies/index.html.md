---
layout: markdown_page
title: "Vacancies"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Introduction

For a listing of open vacancies, please see the [vacancies section on the Jobs page](/jobs#vacancies). Vacancies are maintained in Greenhouse based on our [roles](/handbook/hiring/job-families). A vacancy is a temporarily open position, a position description is a permanent description that also applies to existing people with that title. Don't use vacancy and position description interchangeably. The relevant parts of a position description are copy-pasted to Greenhouse to open a vacancy.

## Vacancy Creation Process

If you want to hire for a position you will need to follow the steps below.
A vacancy needs to be opened in Greenhouse before we can start interviewing.

1. Review the position description in the `/roles` folder and update, if needed, by making a merge request. If there is no position description, follow the steps to [create a new role](/handbook/hiring/job-families/#new-role-creation), then continue. The recruiters will use the relevant elements from this position description to open the role in Greenhouse.
  * The position description should include:
    * Responsibilities
    * Specialty (e.g. Gitaly, Backend)
    * Level
    * Location (e.g. Anywhere, EMEA, Americas)
    * Requirements
    * Hiring Process, also update the internal [hiring process repo](https://gitlab.com/gitlab-com/people-ops/hiring-processes)

1. Ensure the proper approvals are in place to hire for the role.
  * The role must be listed on the "Hiring Forecast" (found on the Google Drive).
  * A compensation benchmark for the role should be set but working with the people-ops team.
  * If the role is not listed on the "Hiring Forecast," the hiring manager will obtain approval from the Executive team member. The Executive team member escalates to the CFO and/or the CEO.
  * Roles listed on the Hiring Forecast will have a one of the following statuses:
    - Open - vacancy is open and we are actively sourcing and hiring for the role
    - Closed - no vacancy, not actively sourcing or hiring for the role
    - Filled - vacancy has been filled by internal or external candidate
    - Draining - vacancy is closed, no longer accepting new candidates, but will review existing candidates through the hiring process and fill the role if a qualified candidate is identified from existing candidates
    - Pending Approval - vacancy has been suggested by executive team member and is pending board, CEO, or CFO approval.

TBD

To find which recruiter works on what role and what roles are currently open, search the Google Drive for "2018 Hiring Listing".

### Posting the Role in Greenhouse

TBD

All vacancies must be posted on our careers page for at least 3 business days before we can close it or make an offer; this includes all new positions and [promotions](/handbook/people-operations/promotions-transfers/#promotions). If a vacancy has been opened for at least 3 business days and has 50 or more applicants, the recruiting team will close the role to new applicants at that time and reopen only if and when we need more applicants.

## Sourcing, Screening, and Resume Submittal

1. When a vacancy is opened a recruiter conducts an intake call with the hiring manager to get details about the position.
1. Vacancy is posted in Greenhouse and on relevant internet job boards.
1. Recruiter may conduct a calibration exercise with the hiring manager by presenting 2-3 unscreened resumes to make sure they are identifying the right skills and will adjust accordingly.
1. Recruiter conducts direct sourcing efforts, reviews resumes, maintains the status of applicants, sets dispositions, and will screen on average 8-10 candidates.
1. Once candidates have been identified and screened, recruiter will submit the top 4-5 qualified applicants to the hiring manager or designee for review.
1. Manager will select which candidates they are interested in interviewing.

## Publicize the Vacancy

The manager should always ask the team for passive referrals for open positions. GitLab team members can refer candidates through our [referral program](/handbook/incentives/#referral-bonuses).

The employment team will **always** publicize the vacancy through the following means:

1. Tweet the new vacancy post with the help of the content marketing manager and team.
1. Request "soft” referrals by encouraging all GitLab team members to post links to the jobs site on their LinkedIn profiles.
1. [Who's Hiring](https://news.ycombinator.com/ask): On the first of the month, include a comment for GitLab in the Hacker News thread of "Who's Hiring" . Template text:
`REMOTE GitLab - We're hiring for developers, designers, product managers, site reliability engineers, and many more roles, see https://about.gitlab.com/jobs/ We're an all-remote company so everyone can participate and contribute equally. GitLab is an open-core application for the whole DevOps lifecycle with over 2000 contributors.` [Example comment](https://news.ycombinator.com/item?id=16967553)

**Note**: The employment team may advertise the vacancy through the following sites and is open to posting to more, in order to widely publish a variety of vacancies:

1. [Alumni post](https://news.ycombinator.com/jobs) as a Y Combinator alumni we can post directly to the front page of Hacker News. The EA vault has credentials so ask an EA to post. Template text: `GitLab (YC W15, All-remote) is hiring XXX and more`. [Example](https://news.ycombinator.com/item?id=16854653)
1. [LinkedIn](https://www.linkedin.com/) (Able to post 3 vacancies simultaneously, please mention to employment team if you want your role listed here)
1. [StackOverflow](http://stackoverflow.com/jobs) (Able to post 3 vacancies simultaneously, please mention to employment team if you want your role listed here)
1. [TechLadies](https://www.hiretechladies.com/) (Able to post 4 roles simultaneously, please mention to employment team if you want your role listed here; at this time we are not posting engineering roles to TechLadies)
1. [PowerToFly](https://powertofly.com/) (All current vacancies are updated biweekly)
1. [RemoteBase](https://remotebase.io/) (Free; position descriptions are synced directly to our respective position description sites)
1. [WeWorkRemotely](https://weworkremotely.com) ($200 for 30 days, per position, used infrequently)
1. [RemoteOK](https://remoteok.io) ($200 for 90 days, per position, used infrequently)
1. [Indeed Prime](http://www.indeed.com/) (Primarily used for non-engineering roles)
1. [Ruby Weekly](https://rubyweekly.com) ($199 per slot per newsletter, for engineering roles)

When using vacancy links to spread the word about our current vacancies, in order to keep data accurate, we can create specific tracking links through Greenhouse in order to include the specific source of different job boards, etc. To learn more about how to create the tracking links for jobs, please [see the Greenhouse help article](https://support.greenhouse.io/hc/en-us/articles/201823760-Create-a-Tracking-Link-for-Your-Job-Board).

## Sourcing for Open Positions

For difficult or hard-to-fill positions, the employment team will use available tools to source for additional candidates. Please communicate with the employment team if sourcing is needed for a strategic, specialized, or difficult to fill position. In addition, managers should also reach out to their own network for candidates and referrals. It is common for candidates to respond more frequently to those who they know are the hiring manager. One superpower of great managers is having a strong network of talent from which to source.

Recruiters dedicate Fridays as "sourcing day". They use various tools to identify talent by pro-actively reaching out to candidates for opportunities. In addition, they will partner with hiring managers to participate in sourcing to ensure they are targeting the right skill sets.

## Closing a vacancy

To close a vacancy:

1. The employment team will clear the pipeline of candidates in all stages of application and notify the candidates that the position has been either filled or closed. Consider rejecting promising candidates with the reason `Future Interest` and making them a prospect so we can reconsider them in the future. You can also add various tags to the candidates, which makes it easier to find them in Greenhouse later on if you are recruiting for the same or a similar position. You can also set a reminder for a candidate if you anticipate reopening the role at a later date.
1. Ask a Greenhouse admin (ideally your recruiter or coordinator) to close the position in Greenhouse.

If the position was posted on any job site (i.e. Stack Overflow, PowerToFly) the employment team will email the partner or remove the position from that site.
